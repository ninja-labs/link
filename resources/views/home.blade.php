<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="/css/iconfont.css" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="/css/main.css">

        <script src="/js/app.js"></script>

        <link rel="shortcut icon" href="/favicon.ico">
        <link rel="icon" type="image/png" href="/favicon.png" sizes="32x32">
        <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
    </head>
    <body>
        <div class="flex flex-col items-center justify-center min-h-full py-16">
            <a href="/" class="flex flex-col items-end">
                <div class="text-5xl">
                    remember
                </div>
                <div class="text-xl">
                    .link
                </div>
            </a>
            @if (count($errors->all()))
                <div class="mt-16 md:rounded-lg bg-red-100 text-red-600 p-8 self-stretch">
                    <div class="font-bold mb-4">
                        There were some errors!
                    </div>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (session('success'))
                <div class="mt-16 md:rounded-lg bg-green-100 text-green-600 p-8 self-stretch">
                    <div class="font-bold mb-4">
                        Success!
                    </div>
                    {{ session('success') }}
                </div>
            @endif
            <form method="POST" action="{{ route('links.store') }}" class="mt-16 flex flex-row items-center justify-center">
                {{ csrf_field() }}
                <input type="text" name="destination" placeholder="Type in your link..." value="{{ old('destination') }}" class="p-4 border rounded-l-lg
                @if($errors->has('destination'))
                    border-red-500
                @endif
                ">
                <button type="submit" class="rounded-r-lg border-green-500 font-bold text-green-500 p-4 border transition hover:text-white hover:bg-green-500">Convert</button>
            </form>

            @if (isset($link))
                <div class="rounded-lg md:border-green-200 md:border-2 p-8 mt-16 container">
                    <div class="font-bold mb-4 text-green-500">
                        Here is your link!
                    </div>

                    <form action="{{ route('links.update', $link) }}" method="POST">
                        @method('PUT')
                        @csrf
                        <pre class="bg-gray-200 md:rounded-lg py-4 px-8 my-4 -mx-8 md:mx-0"><code class="table w-full"><label class="table-cell">{{ route('links.redirect', ['source' => '']) }}/</label><div class="w-full table-row md:table-cell"><input id="link-input" class="bg-transparent w-full" type="text" name="source" value="{{ old('source') ?? $link->source }}"></div></code></pre>
                        <div class="flex flex-row justify-end items-center cursor-pointer select-none copy-link font-bold text-gray-500" data-clipboard-text="{{ route('links.redirect', [ 'source' => $link->source ]) }}">
                            <i class="feather icon-clipboard mr-2"></i>
                            Copy to clipboard
                        </div>
                        <div class="mt-4">
                            You can now edit your link so that it is better memorizable.
                        </div>
                        <button type="submit" class="mt-4 rounded-lg border-green-500 font-bold text-green-500 p-4 border transition hover:text-white hover:bg-green-500">Save changes</button>
                    </form>
                </div>
            @endif
        </div>
    </body>
</html>
