<?php

namespace App\Http\Controllers;

use App\Link;
use Illuminate\Http\Request;
use Faker\Factory as FakerFactory;

class LinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!starts_with($request->get('destination'), 'http')) {
            $request->merge([ 'destination' => 'http://' . $request->get('destination') ]);
        }

        $validated = $this->validate($request, [
            'destination' => 'url|required'
        ]);

        $validated['session_id'] = session()->getId();
        $validated['source'] = null;

        $tries = 0;
        $length = 3;

        while($validated['source'] ===  null || Link::where('source', $validated['source'])->count() != 0) {

            if($tries > 0 && $tries % 1000 === 0) {
                $length = $length + 1;
            }

            $validated['source'] = Link::generate($length);
            \Log::info($validated['source']);

            $tries++;

        }

        $link = Link::create($validated);

        return redirect()->route('links.show', $link);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function show(Link $link)
    {
        if($link->session_id === session()->getId()) {
            return view('home', [ 'link' => $link ]);
        }

        return view('home')->withErrors([ 'This is not one of your links. You can not edit it!' ]);
    }

    public function redirect($source) {
        $link = Link::where('source', $source)->get()->last();

        return redirect($link->destination);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function edit(Link $link)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Link $link)
    {
        if($link->session_id != session()->getId()) {
            return redirect()->back()->withErrors([ 'This link is not yours. You can not change it.' ]);
        }

        $this->validate($request, [
            'source' => 'required|alpha_dash|unique:links'
        ]);

        $link->source = $request->input('source');
        $link->update();

        return redirect()->back()->with('success', 'Updated link.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function destroy(Link $link)
    {
        //
    }
}
