<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $fillable = [ 'source', 'destination', 'session_id' ];

    public static function generate($length) {
        $words = config('words');

        $link = collect();

        for($i = 0; $i < $length; $i++) {
            $link[] = \Arr::random($words);
        }

        return $link->join('-');
    }
}
